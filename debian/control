Source: chasen
Priority: optional
Section: misc
Maintainer: "Natural Language Processing (Japanese)" <team+pkg-nlp-ja@tracker.debian.org>
Uploaders: NOKUBI Takatsugu <knok@daionet.gr.jp>,
           Hideki Yamane <henrich@debian.org>
Build-Depends: debhelper (>= 11),
               debhelper-compat (= 11),
               darts (>= 0.3.2), perl,
Standards-Version: 4.2.1
Homepage: https://chasen-legacy.osdn.jp/
Vcs-Git: https://salsa.debian.org/nlp-ja-team/chasen.git
Vcs-Browser: https://salsa.debian.org/nlp-ja-team/chasen

Package: libchasen-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libchasen2 (= ${binary:Version}), libc6-dev, ${misc:Depends}
Replaces: chasen-dev
Conflicts: chasen-dev
Description: Japanese Morphological Analysis System (libraries and headers)
 ChaSen is a morphological analysis system. It can segment and
 tokenize Japanese text string, and can output with many additional
 information (pronunciation, semantic information, and others).
 .
 You can use ChaSen library to put ChaSen's module into other programs.

Package: libchasen2
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libchasen0, libchasen0c2
Replaces: libchasen0, libchasen0c2
Recommends: naist-jdic-utf8 | naist-jdic | ipadic 
Description: Japanese Morphological Analysis System (shared libraries)
 ChaSen is a morphological analysis system. It can segment and
 tokenize Japanese text string, and can output with many additional
 information (pronunciation, semantic information, and others).
 .
 This package contains shared libraries for ChaSen.

Package: chasen
Architecture: any
Depends: ${shlibs:Depends}, naist-jdic-utf8 (>= 1:0.4.3-3) | naist-jdic (>= 1:0.4.3-3) | ipadic (>= 2.6.3), ${misc:Depends}
Description: Japanese Morphological Analysis System
 ChaSen is a morphological analysis system. It can segment and
 tokenize Japanese text string, and can output with many additional
 information (pronunciation, semantic information, and others).
 .
 It will print the result of such an operation to the standard output,
 so that it can be either written to a file or further processed.

Package: chasen-dictutils
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Japanese Morphological Analysis System - utilities for dictionary
 ChaSen is a morphological analysis system. It can segment and
 tokenize Japanese text string, and can output with many additional
 information (pronunciation, semantic information, and others).
 .
 This package contains dictionary utilities for ChaSen.
